//TODOO
// 1 - GENERATION DE GRILLE
// 2 - INSERTION DE BASE (aléatoire/par défaut)
// 3 - DELETE
// 4 - ADD
// 5 - VISUALISATION DES VOISINS
// 6 - NAISSANCE
// 7 - DÉCÈS
// 8 - RUN 

const divResultat = document.querySelector('#resultat');

function afficherTab(g){
    let txt="";
    txt += "<table border=1>"
    for(let i = 0;i<size;i++){
        txt+="<tr>";
        for(let j = 0;j<size;j++){
            if(g[i][j]==1){
                txt+="<td class='black'></td>";
            }else{
                txt+="<td></td>";
            }

        }
        txt+="</tr>";
    }
    txt+= "</table"
    divResultat.innerHTML = txt;
}




//Génération du monde
var grid = [];
var gridclone = []; 
var size = 0;

function Generated1(g){
    let ligne = [];
    for(i = 0; i<size; i++){
        for(j = 0; j<size; j++){
            ligne.push(0);
        }
        g.push(ligne);
        ligne = [];
    }
}


//Insertion de base par défaut
function Base1(g){
    g[2][1] = 1;
    g[2][2] = 1;
    g[2][3] = 1;
}

function Base2(g){
    g[3][2] = 1;
    g[3][3] = 1;
    g[3][4] = 1;
    g[3][5] = 1;
}
function Base3(g){
    g[4][3] = 1;
    g[4][4] = 1;
    g[4][5] = 1;
    g[4][6] = 1;
    g[4][7] = 1;
}
function Base4(g){
    g[1][2] = 1;
    g[2][2] = 1;
    g[3][2] = 1;
    g[2][3] = 1;
    g[3][3] = 1;
}
//Add
function Add( y , x){
    gridclone[y][x] = 1;
}

//Delete
function Delete( y , x){
    gridclone[y][x] = 0;
}

//Voisins

function voisin(x,y){
    let compteur =0;
    for(let i = -1; i<2;i++){
        for(let j = -1; j<2;j++){
            if(i==0 && j==0 || i+x<0 || j+y<0 || j+y>=size || i+i>=size ){
                continue;
            }else if(grid[i+x][j+y]==1){
                compteur++;
            }
        }
    }
    return compteur;
} 

function voisins(x,y){
    let compteur = 0;
    for(let i = -1; i<2;i++){
        for(let j = -1; j<2;j++){
            if(i!=0 || j!=0){
                if( x-1>=0 && y-1>=0 && y+1<size && x+1<size && grid[i+x][j+y]==1){
                    compteur+=1
                }
            }
        }
    }

    return compteur;
}


var l = [];

function jeuDeLaVie1(){
    size = 5;
    Generated1(grid);
    Generated1(gridclone)
    Base1(grid);
    Base1(gridclone);
    console.log("Jour " + 1)
    console.log(gridclone)
    for(let n = 0; n<5; n++){
        if(n>0){
            Generated1(gridclone)
            Base1(gridclone)
        }

        for(let i = 0; i<grid.length;i++){
            for (let j = 0; j < grid[i].length; j++) {
                if(voisins(i,j)==3){
                    Add(i,j);
                }else if(grid[i][j]==1 && voisins(i, j) > 3 || voisins(i, j) < 2){
                    Delete(i,j);
                }
            }
        }
        console.log("Jour " + (n+2))
        
        console.log(gridclone);
        setInterval(afficherTab(gridclone),2000);
        grid=gridclone
        gridclone=[]
    }
}

function jeuDeLaVie2(){
    size = 8;
    Generated1(grid);
    Generated1(gridclone)
    Base2(grid);
    Base2(gridclone);
    console.log("Jour " + 1)
    console.log(gridclone)
    for(let n = 0; n<4; n++){
        if(n>0){
            Generated1(gridclone)
            Base2(gridclone)
        }

        for(let i = 0; i<grid.length;i++){
            for (let j = 0; j < grid[i].length; j++) {
                if(voisins(i,j)==3){
                    Add(i,j);
                }else if(grid[i][j]==1 && voisins(i, j) > 3 || voisins(i, j) < 2){
                    Delete(i,j);
                }
            }
        }
        console.log("Jour " + (n+2))
        console.log(gridclone);
        setInterval(afficherTab(gridclone),2000);
        grid=gridclone
        gridclone=[]
    }
}
function jeuDeLaVie3(){
    size = 10;
    Generated1(grid);
    Generated1(gridclone)
    Base3(grid);
    Base3(gridclone);
    console.log("Jour " + 1)
    console.log(gridclone)
    for(let n = 0; n<8; n++){
        if(n>0){
            Generated1(gridclone)
            Base3(gridclone)
        }

        for(let i = 0; i<grid.length;i++){
            for (let j = 0; j < grid[i].length; j++) {
                if(voisins(i,j)==3){
                    Add(i,j);
                }else if(grid[i][j]==1 && voisins(i, j) > 3 || voisins(i, j) < 2){
                    Delete(i,j);
                }
            }
        }
        console.log("Jour " + (n+2))
        console.log(gridclone);
        grid=gridclone
        gridclone=[]
    }
}

function jeuDeLaVie4(){
    size = 5;
    Generated1(grid);
    Generated1(gridclone)
    Base4(grid);
    Base4(gridclone);
    console.log("Jour " + 1)
    console.log(gridclone)
    for(let n = 0; n<8; n++){
        if(n>0){
            Generated1(gridclone)
            Base4(gridclone)
        }

        for(let i = 0; i<grid.length;i++){
            for (let j = 0; j < grid[i].length; j++) {
                if(voisins(i,j)==3){
                    Add(i,j);
                }else if(grid[i][j]==1 && voisins(i, j) > 3 || voisins(i, j) < 2){
                    Delete(i,j);
                }
            }
        }
        console.log("Jour " + (n+2))
        console.log(gridclone);
        afficherTab(gridclone);
        grid=gridclone
        gridclone=[]
    }
}


setInterval(jeuDeLaVie1(),1000);
//jeuDeLaVie2()
//jeuDeLaVie3()
//jeuDeLaVie4()



