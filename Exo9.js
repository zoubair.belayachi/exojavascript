function unCarré(vars){
    let list1 = [];
    let ligne = [];
    for(i=0; i<vars;i++){
        for(j=0;j<vars;j++){
            if( i == 0 || i == vars-1 || j==0 || j == vars-1){
                ligne.push(1);
            }else
                ligne.push(0);
        }
        list1.push(ligne);
        ligne = [];
    }
    return list1;
}

console.log(unCarré(5));