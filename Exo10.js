var listRubiks = [[1,0,2],[0, 0, 0],[3, 0, 4]];

function rubiks(list, num){
    let all = [];
    let ligne = [];
    for(z = 0; z<num - 1;z++){
        for(i = 0; i<list.length; i++){
            for(j = 0; j<list.length; j++){
                ligne.push(list[list.length - (j+1)][i]);
            }
            all.push(ligne);
            ligne = []
        }
        list = all;
        all = [];
    }
    return list;
}

console.log(rubiks(listRubiks, 2));